Rails.application.routes.draw do
  post '/photos' => 'photos#create'

  devise_for :users, controllers: { registrations: 'registrations' }

  get 'profile' => 'users#show', as: 'profile'
  get 'dashboard' => 'dashboard#index', as: 'dashboard'

  resources :questions
  resources :games
  resources :subjects

  root to: "home#index"

  mount Ckeditor::Engine => '/ckeditor'

  namespace :api do
    namespace :v1 do
      resources :users, only: [:index, :show]
      resources :questions, only: [:index, :show] do
        collection do
          get 'get_related'
        end
      end
      resources :subjects, only: [:index, :show]
      resources :school_grades, only: [:index, :show]
      resources :school_types, only: [:index, :show]
      resources :markers, only: [:index, :show]
      resources :games, except: [:new, :edit] do
        collection do
          get 'list_names'
          get 'by_pin/:pin' => 'games#by_pin'
        end

        member do
          post 'check_password'
        end
      end
    end
  end

  # The priority is based upon order of creation: first created -> highest priority.
  # See how all your routes lay out with "rake routes".

  # You can have the root of your site routed with "root"
  # root 'welcome#index'

  # Example of regular route:
  #   get 'products/:id' => 'catalog#view'

  # Example of named route that can be invoked with purchase_url(id: product.id)
  #   get 'products/:id/purchase' => 'catalog#purchase', as: :purchase

  # Example resource route (maps HTTP verbs to controller actions automatically):
  #   resources :products

  # Example resource route with options:
  #   resources :products do
  #     member do
  #       get 'short'
  #       post 'toggle'
  #     end
  #
  #     collection do
  #       get 'sold'
  #     end
  #   end

  # Example resource route with sub-resources:
  #   resources :products do
  #     resources :comments, :sales
  #     resource :seller
  #   end

  # Example resource route with more complex sub-resources:
  #   resources :products do
  #     resources :comments
  #     resources :sales do
  #       get 'recent', on: :collection
  #     end
  #   end

  # Example resource route with concerns:
  #   concern :toggleable do
  #     post 'toggle'
  #   end
  #   resources :posts, concerns: :toggleable
  #   resources :photos, concerns: :toggleable

  # Example resource route within a namespace:
  #   namespace :admin do
  #     # Directs /admin/products/* to Admin::ProductsController
  #     # (app/controllers/admin/products_controller.rb)
  #     resources :products
  #   end
end
