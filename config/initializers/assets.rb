Rails.application.config.assets.precompile += %w( application.css chosen.min.css dashboard.css.scss
    fonts.css.scss home.css questions.css users.css.scss application.js.coffee chosen.jquery.min.js 
    holder.js turbolinks.js jquery.validate.min.js games.js dashboard.js dashboard.js.coffee home.js map_functions.js.coffee questions.js users.js.coffee
    gritter.png gritter-close.png success.png notice.png warning.png ie-spacer.gif jquery-1.11.0.min.js )

Rails.application.config.assets.precompile += Ckeditor.assets

Rails.application.config.assets.precompile << /\.(?:svg|eot|woff|ttf)$/