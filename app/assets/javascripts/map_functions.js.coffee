root = exports ? this

root.icons =
  gold_marker : [
      url: '/assets/blue-coin-marker.png'
      size: new google.maps.Size(36, 50)
      anchor: new google.maps.Point(17,47)
    ,
      url: '/assets/green-coin-marker.png'
      size: new google.maps.Size(36, 50)
      anchor: new google.maps.Point(17,47)            
    ,
      url: '/assets/grey-coin-marker.png'
      size: new google.maps.Size(36, 50)
      anchor: new google.maps.Point(17,47)            
    ,
      url: '/assets/orange-coin-marker.png'
      size: new google.maps.Size(36, 50) 
      anchor: new google.maps.Point(17,47)           
    ,
      url: '/assets/pink-coin-marker.png'
      size: new google.maps.Size(36, 50)
      anchor: new google.maps.Point(17,47)
    ]

  big_gold_marker : [
      url: '/assets/coin-marker-blue.png'
      size: new google.maps.Size(42, 52)
      anchor: new google.maps.Point(21,51)
    ,
      url: '/assets/coin-marker-pink.png'
      size: new google.maps.Size(42, 52)
      anchor: new google.maps.Point(21,51)            
    ,
      url: '/assets/coin-marker-orange.png'
      size: new google.maps.Size(42, 52)
      anchor: new google.maps.Point(21,51)            
    ,
      url: '/assets/coin-marker-green.png'
      size: new google.maps.Size(42, 52) 
      anchor: new google.maps.Point(21,51)           
    ,
      url: '/assets/coin-marker-purple.png'
      size: new google.maps.Size(42, 52)
      anchor: new google.maps.Point(21,51)
    ,
      url: '/assets/coin-marker-black.png'
      size: new google.maps.Size(42, 52)
      anchor: new google.maps.Point(21,51)
    ]

  question_marker : [
      url: '/assets/question-marker-blue.png'
      size: new google.maps.Size(36, 50)
      anchor: new google.maps.Point(17,47)
    ,
      url: '/assets/question-marker-green.png'
      size: new google.maps.Size(36, 50)
      anchor: new google.maps.Point(17,47)            
    ,
      url: '/assets/question-marker-grey.png'
      size: new google.maps.Size(36, 50)
      anchor: new google.maps.Point(17,47)            
    ,
      url: '/assets/question-marker-orange.png'
      size: new google.maps.Size(36, 50) 
      anchor: new google.maps.Point(17,47)           
    ,
      url: '/assets/question-marker-pink.png'
      size: new google.maps.Size(36, 50)
      anchor: new google.maps.Point(17,47)
    ]

  big_question_marker : [
      url: '/assets/question-marker-blue-2.png'
      size: new google.maps.Size(42, 52)
      anchor: new google.maps.Point(21,51)
    ,
      url: '/assets/question-marker-green-2.png'
      size: new google.maps.Size(42, 52)
      anchor: new google.maps.Point(21,51)            
    ,
      url: '/assets/question-marker-purple-2.png'
      size: new google.maps.Size(42, 52)
      anchor: new google.maps.Point(21,51)            
    ,
      url: '/assets/question-marker-orange-2.png'
      size: new google.maps.Size(42, 52) 
      anchor: new google.maps.Point(21,51)           
    ,
      url: '/assets/question-marker-pink-2.png'
      size: new google.maps.Size(42, 52)
      anchor: new google.maps.Point(21,51)
    ,
      url: '/assets/question-marker-black-2.png'
      size: new google.maps.Size(42, 52)
      anchor: new google.maps.Point(21,51)
    ]

  flag_marker : [
      url: '/assets/flag-marker-blue.png'
      size: new google.maps.Size(64, 36)
      anchor: new google.maps.Point(31,36)
    ,
      url: '/assets/flag-marker-green.png'
      size: new google.maps.Size(64, 36)
      anchor: new google.maps.Point(31,36)            
    ,
      url: '/assets/flag-marker-purple.png'
      size: new google.maps.Size(64, 36)
      anchor: new google.maps.Point(31,36)            
    ,
      url: '/assets/flag-marker-orange.png'
      size: new google.maps.Size(64, 36) 
      anchor: new google.maps.Point(31,36)           
    ,
      url: '/assets/flag-marker-pink.png'
      size: new google.maps.Size(64, 36)
      anchor: new google.maps.Point(31,36)
    ,
      url: '/assets/flag-marker-black.png'
      size: new google.maps.Size(64, 36)
      anchor: new google.maps.Point(31,36)
    ]

  pin_marker : [
      url: '/assets/pin-blue.png'
      size: new google.maps.Size(18, 24)
      anchor: new google.maps.Point(9,24)
    ,
      url: '/assets/pin-green.png'
      size: new google.maps.Size(18, 24)
      anchor: new google.maps.Point(9,24)            
    ,
      url: '/assets/pin-purple.png'
      size: new google.maps.Size(18, 24)
      anchor: new google.maps.Point(9,24)            
    ,
      url: '/assets/pin-orange.png'
      size: new google.maps.Size(18, 24) 
      anchor: new google.maps.Point(9,24)           
    ,
      url: '/assets/pin-pink.png'
      size: new google.maps.Size(18, 24)
      anchor: new google.maps.Point(9,24)
    ,
      url: '/assets/pin-black.png'
      size: new google.maps.Size(18, 24)
      anchor: new google.maps.Point(9,24)
    ]

  start_marker : [
      url: '/assets/flag-marker-blue.png'
      size: new google.maps.Size(64, 36)
      anchor: new google.maps.Point(31,36)
    ]

  finish_marker : [
      url: '/assets/flag-marker-green.png'
      size: new google.maps.Size(64, 36)
      anchor: new google.maps.Point(31,36)
    ]


root.initializeMap = (elementId, mapOptions)->
  container = $("##{elementId}")
  if container.length > 0
    styles = [
                {
                    "featureType": "water",
                    "stylers": [
                        {
                            "saturation": 43
                        },
                        {
                            "lightness": -11
                        },
                        {
                            "hue": "#0088ff"
                        }
                    ]
                }
            ]

    mapOptions["styles"] = styles

    return new google.maps.Map(container.get(0), mapOptions)

root.infowindow = new google.maps.InfoWindow();

root.getKeyByValue = (object, value)->
  keys = Object.keys object

  i = 0
  while i < keys.length
    if object[keys[i]] is value
      return keys[i]
    i++

root.repopulateMarkerTypes = (element, use_prompt, current_marker = "")->
  marker_types = ["gold", "question"]
  start_marker_length = $("input.marker-type").filter(()-> return this.value is 'start').length
  finish_marker_length = $("input.marker-type").filter(()-> return this.value is 'finish').length

  if (start_marker_length < 1 || current_marker is 'start')
    marker_types.push("start")

  if (finish_marker_length < 1 || current_marker is 'finish')
    marker_types.push("finish")

  $("#{element} option").remove();

  if use_prompt
    $("#{element}").append("<option value=\"\">- Select type -</option>")

  i = 0
  while i < marker_types.length
    $("#{element}").append("<option value=\"#{marker_types[i]}\">#{marker_types[i]}</option>")
    i++

root.putMarker = (marker_data, marker_type, opts)->
  marker_icons = icons[marker_type]
  number_of_icons = marker_icons.length

  marker = new google.maps.Marker
    position: new google.maps.LatLng(marker_data.lat, marker_data.lng)
    map: map
    draggable: opts.draggable
    icon: marker_icons[0]

  if marker_data.markerFieldId
    content = $("#infowindow-content").html().replace("{{marker-field-id}}", marker_data.markerFieldId)
                .replace("{{title}}", marker_data.title)
                .replace("{{type}}", marker_type)

    google.maps.event.addListener marker, "click", ((marker, content) ->
      ->
        infowindow.setContent content
        infowindow.open map, marker
        affectedMarkerFieldId = getKeyByValue(insertedMarkers, marker)
        markerType = $("#game_markers_attributes_#{affectedMarkerFieldId}_marker_type").val()
        repopulateMarkerTypes("#infowindow-#{affectedMarkerFieldId} .existing-marker-marker-type", false, markerType)
        $("#infowindow-#{affectedMarkerFieldId} .existing-marker-marker-type").val(markerType)
        $("#infowindow-#{affectedMarkerFieldId} .marker-description").text("this marker indicating location of a #{markerType}")
        $("#infowindow-#{affectedMarkerFieldId} h3.panel-title").text("#{markerType} marker")
        return
    )(marker, content)

  root.bounds.extend(marker.position)

  marker


root.putMarkers = (marker_data, marker_type, opts)->
  markers = []
  marker_icons = icons[marker_type]
  number_of_icons = marker_icons.length

  i = 0

  while i < marker_data.length
    marker = putMarker(marker_data[i], marker_type, opts)
    markers.push marker
    i++

  markers


drawPath = ()->
  journey_path_coordinates = $.map markers, (marker)-> 
    return marker.getPosition()

  journey_path = new google.maps.Polyline
    path: journey_path_coordinates
    geodesic: true
    strokeColor: '#FFE268'
    strokeOpacity: 1.0
    strokeWeight: 4

  journey_path.setMap(map);


animateMarker = ()->
  setInterval -> 
    i = 0

    while i < markers.length
      if markers[i].getAnimation() != null
        markers[i].setAnimation(null)
      else
        markers[i].setAnimation(google.maps.Animation.BOUNCE)
      i++
  , 1450


root.bindOnClickEvent = (opts)->
  google.maps.event.addListener map, 'click', (event)->
    latLng = event.latLng
    $("#markers .links .add_fields").click()
    $("#new_marker_latitude, #new_marker_address_latitude").val latLng.lat()
    $("#new_marker_longitude, #new_marker_address_longitude").val latLng.lng()