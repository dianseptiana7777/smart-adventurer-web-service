# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://coffeescript.org/

$(document).ready ->
  directions = new GDirections(map);
  marker_coordinates = [
                        new google.maps.LatLng(-6.8608405, 107.5929371)
                        new google.maps.LatLng(-6.860548701318513, 107.5895619392395)
                        new google.maps.LatLng(-6.861102605814931, 107.59126782417297)
                        new google.maps.LatLng(-6.861400861815368, 107.59270548820496)
                        new google.maps.LatLng(-6.86209324109612, 107.59402513504028)
                      ]

  lineSymbol =
    path: google.maps.SymbolPath.FORWARD_CLOSED_ARROW

  flightPath = new google.maps.Polyline
    path: marker_coordinates,
    icons: [{
            icon: lineSymbol
            offset: '100%'
          }]

  directions.load("from:" + -6.8608405 + ", " + 
                    107.5929371 + 
                    " to:" + 6.860548701318513 + "," + 
                    107.5895619392395, 
                    { getPolyline: true, getSteps: true }); 

  mapOptions =
    center: { lat: -6.8608405, lng: 107.5929371 },
    zoom: 17


  map = new google.maps.Map($("#map").get(0), mapOptions)

  i = 0
  while i < marker_coordinates.length 
    marker = new google.maps.Marker
      position: marker_coordinates[i]
      map: map
      title: 'Click to zoom'

    i++

  flightPath.setMap(map);

  google.maps.event.addListener map, 'click', (event)->
    alert "Latitude: #{event.latLng.lat()}, longitude: #{event.latLng.lng()}"