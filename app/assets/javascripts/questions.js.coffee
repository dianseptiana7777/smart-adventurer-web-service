$(document).ready ->
  $("#question_school_grade_id").chosen { "disable_search_threshold": 4 }
  $("#question_subject_id").chosen { "disable_search_threshold": 4 }
  $("#question_difficulty_level").chosen { "disable_search": true }

  $('.input-group-addon').on 'click', ->
    currentInputFieldValue = $(this).siblings('input').val()
    currentChoice = $(this).text()

    if currentInputFieldValue
      $("#question_correct_answer_#{currentChoice.toLowerCase()}").prop "checked", true
      currentInputGroup = $(this).parent()
      $('.input-group').not(currentInputGroup).removeClass 'has-success'
      currentInputGroup.addClass 'has-success'
      $('#correct-answer-label').text "#{currentChoice}. #{currentInputFieldValue}"
    else
      alert 'You should enter choice text before you can choose it as correct answer'

  $('.choice-field').on 'keyup blur focus', ->
    currentChoice = $(this).siblings('.input-group-addon').text()

    if $(this).parent().hasClass("has-success")
      $('#correct-answer-label').text "#{currentChoice}. #{$(this).val()}"

  $('.btn-scope').on 'click', ->
    isPublic = $(this).data 'is-public'
    $('.btn-scope').not(this).removeClass 'active'
    $(this).addClass('active')
    $("#question_is_public_#{isPublic}").prop('checked', true)

  $('#question_school_grade_id').on 'change', ->
    gradeId = $(this).val()

    $.ajax
      url: '/subjects'
      type: 'GET'
      dataType: 'json'
      data:
        grade_id: gradeId
      beforeSend: ->
        console.log 'before send'
      success: (data, status)-> 
        $('#question_subject_id option').remove()
        $('#question_subject_id').append("<option></option>")

        if data.length > 0
          i = 0
          while i < data.length
            $('#question_subject_id').append("<option value=\"#{data[i].id}\">#{data[i].name}</option>")
            i++
          $('#question_subject_id').attr("data-placeholder", "- Select subject -").prop("disabled", false)
        else
          $('#question_subject_id').attr("data-placeholder", "- No subjects -")
          $('#question_subject_id').attr("data-placeholder", "- Select subject -").prop("disabled", true)

        $('#question_subject_id').trigger("chosen:updated")
