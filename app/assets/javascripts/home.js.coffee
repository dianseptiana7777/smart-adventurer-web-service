# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://coffeescript.org/
root = exports ? this

marker_data = 
  gold_markers : [ 
    { lat: -6.871552106388241, lng: 107.55975723266602, title: 'gold marker 1', content: 'gold content 1' },
    { lat: -6.868654816279271, lng: 107.59237289428711, title: 'gold marker 2', content: 'gold content 2' },
    { lat: -6.914668509783146, lng: 107.56782531738281, title: 'gold marker 3', content: 'gold content 3' },
    { lat: -6.928982968564721, lng: 107.61262893676758, title: 'gold marker 4', content: 'gold content 4' },
    { lat: -6.911430655357397, lng: 107.63168334960938, title: 'gold marker 5', content: 'gold content 5' }
  ]

  big_gold_markers : [ 
    { lat: -6.895922729466397, lng: 107.55426406860352, title: 'big gold marker 1', content: 'big gold content 1' },
    { lat: -6.8824593930942894, lng: 107.5971794128418, title: 'big gold marker 2', content: 'big gold content 2' },
    { lat: -6.907170286796667, lng: 107.60009765625, title: 'big gold marker 3', content: 'big gold content 3' },
    { lat: -6.92046250968232, lng: 107.6601791381836, title: 'big gold marker 4', content: 'big gold content 4' },
    { lat: -6.933754358291913, lng: 107.6411247253418, title: 'big gold marker 5', content: 'big gold content 5' },
    { lat: -6.871040821181136, lng: 107.67047882080078, title: 'big gold marker 6', content: 'big gold content 6' }
  ]

  question_markers : [ 
    { lat: -6.884845328666755, lng: 107.57469177246094, title: 'question marker 1', content: 'question content 1' },
    { lat: -6.87479023326337, lng: 107.62155532836914, title: 'question marker 2', content: 'question content 2' },
    { lat: -6.897797340910827, lng: 107.66412734985352, title: 'question marker 3', content: 'question content 3' },
    { lat: -6.879050892902536, lng: 107.6993179321289, title: 'question marker 4', content: 'question content 4' },
    { lat: -6.913134792031106, lng: 107.71322250366211, title: 'question marker 5', content: 'question content 5' }
  ]

  big_question_markers : [ 
    { lat: -6.898649434569228, lng: 107.5839614868164, title: 'big question marker 1', content: 'big question content 1' },
    { lat: -6.931879889517204, lng: 107.58705139160156, title: 'big question marker 2', content: 'big question content 2' },
    { lat: -6.923700302184781, lng: 107.54705429077148, title: 'big question marker 3', content: 'big question content 3' },
    { lat: -6.940570545500536, lng: 107.68386840820312, title: 'big question marker 4', content: 'big question content 4' },
    { lat: -6.903080296873566, lng: 107.72987365722656, title: 'big question marker 5', content: 'big question content 5' },
    { lat: -6.8787100415377065, lng: 107.72832870483398, title: 'big question marker 6', content: 'big question content 6' }
  ]

  flag_markers : [ 
    { lat: -6.932391109013438, lng: 107.55838394165039, title: 'flag marker 1', content: 'flag content 1' },
    { lat: -6.915179747927456, lng: 107.60147094726562, title: 'flag marker 2', content: 'flag content 2' },
    { lat: -6.8901284290218925, lng: 107.61983871459961, title: 'flag marker 3', content: 'flag content 3' },
    { lat: -6.8797325948982895, lng: 107.6443862915039, title: 'flag marker 4', content: 'flag content 4' },
    { lat: -6.891662221327114, lng: 107.68678665161133, title: 'flag marker 5', content: 'flag content 5' },
    { lat: -6.912964378640257, lng: 107.67906188964844, title: 'flag marker 6', content: 'flag content 6' }
  ]

  pin_markers : [ 
    { lat: -6.936651249910534, lng: 107.54293441772461, title: 'pin marker 1', content: 'pin content 1' },
    { lat: -6.92131456249441, lng: 107.57400512695312, title: 'pin marker 2', content: 'pin content 2' },
    { lat: -6.893536849627338, lng: 107.63957977294922, title: 'pin marker 3', content: 'pin content 3' },
    { lat: -6.8797325948982895, lng: 107.66996383666992, title: 'pin marker 4', content: 'pin content 4' },
    { lat: -6.903761964315963, lng: 107.6986312866211, title: 'pin marker 5', content: 'pin content 5' },
    { lat: -6.939888931220029, lng: 107.65605926513672, title: 'pin marker 6', content: 'pin content 6' }
  ]

container = 'map'
mapOptions =
  panControl: false
  zoomControl: true
  mapTypeControl: false
  scaleControl: false
  streetViewControl: false
  overviewMapControl: false
  center:
    lat: -6.9033101
    lng: 107.642621
  zoom: 13

root.bounds = new google.maps.LatLngBounds();

$(document).ready ->
  root.map = initializeMap(container, mapOptions)
  rootbounds = new google.maps.LatLngBounds();
  gold_markers = putMarkers(marker_data.gold_markers, "big_gold_marker", { draggable: false })
  big_gold_markers = putMarkers(marker_data.big_gold_markers, "gold_marker", { draggable: false })
  map.fitBounds(bounds)