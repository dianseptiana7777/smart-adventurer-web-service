root = exports ? this

root.bounds = new google.maps.LatLngBounds();
root.insertedMarkers = {}

copyValue = ()->
  if $("#new_marker_use_address").is(":checked")
    elements = $("#marker-modal .marker-field").not("#new_marker_latitude, #new_marker_longitude")
  else
    elements = $("#marker-modal .marker-field").not("#new_marker_address_latitude, #new_marker_address_longitude")

  i = 0
  while i < elements.length
    markerFieldId = $(elements[i]).data("marker-field-id")
    fieldType = $(elements[i]).attr("id").replace("new_marker_", "")
      .replace("address_latitude", "latitude")
      .replace("address_longitude", "longitude")
    $("#game_markers_attributes_#{markerFieldId}_#{fieldType}").val $(elements[i]).val()

    i++

$.validator.setDefaults submitHandler: ->
  copyValue()
  if $("#new_marker_use_address").is(":checked")
    lat = $("#new_marker_address_latitude").val()
    lng = $("#new_marker_address_longitude").val()
  else
    lat = $("#new_marker_latitude").val()
    lng = $("#new_marker_longitude").val()
  type = [$("#new_marker_marker_type").val(), "marker"].join("_")
  markerFieldId = $("#btn-put-marker").data("marker-field-id")
  markerData =
    lat: lat, lng: lng, title: type.replace("_", " "), markerFieldId: markerFieldId 
    content: "this marker indicating location of a #{type.replace("_marker", "")}"
  insertedMarkers[markerFieldId] = putMarker(markerData, type, { draggable: true })  

  google.maps.event.addListener insertedMarkers[markerFieldId], 'dragend', -> 
    $("#game_markers_attributes_#{markerFieldId}_latitude").val this.getPosition().lat()
    $("#game_markers_attributes_#{markerFieldId}_longitude").val this.getPosition().lng()
  $("#marker-modal").modal "hide"

  resetForm()

resetForm = ()->
  $("#marker-modal .marker-field").val("")
  $("#new_marker_use_address").prop("checked", false)
  $("#new_marker_address, #new_marker_address_latitude, #new_marker_address_longitude").addClass("skip-validate")
  $("#new_marker_latitude, #new_marker_longitude").removeClass("skip-validate")
  $("#address-container").addClass("hide")

initializeFormPage = ()->
  $("form#newMarkerForm").validate({ ignore: '.skip-validate' })

  container = 'map'
  mapOptions =
    scrollwheel: false
    panControl: false
    zoomControl: true
    mapTypeControl: false
    scaleControl: false
    streetViewControl: false
    overviewMapControl: false
    center:
      lat: -6.9033101
      lng: 107.642621
    zoom: 13

  root.map = initializeMap(container, mapOptions)
  map.setOptions({ draggableCursor: "default" })

  google.maps.event.addListener map, 'idle', ->
    latlng = map.getCenter()
    $.ajax
      url: "http://maps.googleapis.com/maps/api/geocode/json?latlng=#{latlng.lat()},#{latlng.lng()}&sensor=true"
      type: "GET"
      dataType: "json"
      success: (data, textStatus, jqXHR) ->
        if data.status is 'ZERO_RESULTS'
          $("#game_address").val('')
        else
          $("#game_address").val(data.results[0].formatted_address)

  markerFields = $("#markers .marker-fields")

  if markerFields.length > 0
    i = 0

    while i < markerFields.length
      markerFieldId = $(markerFields[i]).find("input:first").attr("id").split("_")[3]
      type = $("#game_markers_attributes_#{markerFieldId}_marker_type").val() + "_marker"

      markerData =
        lat: $("#game_markers_attributes_#{markerFieldId}_latitude").val()
        lng: $("#game_markers_attributes_#{markerFieldId}_longitude").val()
        title: type.replace("_", " "), markerFieldId: markerFieldId 
        content: "this marker indicating location of a #{type.replace("_marker", "")}"

      insertedMarkers[markerFieldId] = putMarker(markerData, type, { draggable: true })

      google.maps.event.addListener insertedMarkers[markerFieldId], 'dragend', -> 
        affectedMarkerFieldId = getKeyByValue(insertedMarkers, this)
        $("#game_markers_attributes_#{affectedMarkerFieldId}_latitude").val this.getPosition().lat()
        $("#game_markers_attributes_#{affectedMarkerFieldId}_longitude").val this.getPosition().lng()

      map.fitBounds(bounds)
      setTimeout(
        ()-> root.map.setZoom(14)
      , 50)

      i++

  bindOnClickEvent({ draggable: true })

  input = $("#new_marker_address")
  autocomplete = new google.maps.places.Autocomplete(input.get(0), { types: [] })
  google.maps.event.addListener autocomplete, 'place_changed', ()->
    location = autocomplete.getPlace().geometry.location
    markerFieldId = $("#new_marker_address").data("marker-field-id")
    $("#new_marker_address_latitude, #game_markers_attributes_#{markerFieldId}_marker_latitude").val location.lat()
    $("#new_marker_address_longitude, #game_markers_attributes_#{markerFieldId}_marker_latitude").val location.lng()

  $('#new_marker_address').keydown (e)->
    return false if e.which is 13

  $("#markers").on "cocoon:after-insert", (e, insertedItem)->
    insertedItemId = insertedItem.find('input:first').attr("id").split("_")[3]
    $("#new_marker_latitude, #new_marker_longitude, #new_marker_marker_type, #btn-put-marker, #btn-cancel,
      #new_marker_use_address, #new_marker_address, #new_marker_address_latitude, #new_marker_address_longitude")
        .data "marker-field-id", insertedItemId
    repopulateMarkerTypes("#new_marker_marker_type", true)
    $("#marker-modal").modal "show"

  $("#markers").on "cocoon:before-remove", (e, removedItem)->
    removedItemId = removedItem.find('input:first').attr("id").split("_")[3]
    if insertedMarkers[removedItemId]
      insertedMarkers[removedItemId].setMap(null)
      delete insertedMarkers[removedItemId]

initializeIndexPage = ()->
  gameMaps = $(".game-maps")
  root.bounds = new google.maps.LatLngBounds(null)

  i = 0
  while i < gameMaps.length
    elementId = $(gameMaps[i]).attr("id")
    mapOptions =
      scrollwheel: false
      panControl: false
      zoomControl: true
      mapTypeControl: false
      scaleControl: false
      streetViewControl: false
      overviewMapControl: false
      center:
        lat: -6.9033101
        lng: 107.642621
      zoom: 13
    
    root.map = initializeMap(elementId, mapOptions)
    markers = $(gameMaps[i]).parent().siblings(".marker-data").data("marker-data")

    j = 0
    while j < markers.length
      type = markers[j].marker_type + "_marker"
      markerData = 
        lat: markers[j].latitude, lng: markers[j].longitude
        title: type.replace("_", " ")
        content: "this marker indicating location of a #{type.replace("_marker", "")}"

      marker = putMarker(markerData, type, { draggable: false })
      j++

    setTimeout(
      ()-> 
        root.map.setZoom(14)
        map.fitBounds(bounds);
    , 1000)

    i++

$("#game_use_password").on 'click', ->
  if $(this).is(":checked")
    $(".password-container").removeClass("hide")
  else
    $(".password-container").addClass("hide")

$("#new_marker_latitude, #new_marker_longitude").on "keyup focus blur", ->
  copyValue()

$("#new_marker_marker_type").on "change", ->
  copyValue()

$("#btn-cancel").on 'click', ->
  resetForm()
  markerFieldId = $(this).data("marker-field-id")
  $("#game_markers_attributes_#{markerFieldId}_latitude").closest(".marker-fields")
    .find(".remove_fields").click()
  $("#marker-modal").modal "hide"

$("#new_marker_use_address").on 'click change', ->
  if $(this).is(":checked")
    $("#new_marker_address, #new_marker_address_latitude, #new_marker_address_longitude").removeClass("skip-validate")
    $("#new_marker_latitude, #new_marker_longitude").addClass("skip-validate")
    $("#address-container").removeClass("hide")
  else
    $("#new_marker_address, #new_marker_address_latitude, #new_marker_address_longitude").addClass("skip-validate")
    $("#new_marker_latitude, #new_marker_longitude").removeClass("skip-validate")
    $("#address-container").addClass("hide")  

$(document).on 'click', '.btn-remove-marker', ()->
  removedItemId = $(this).closest('.infowindow-content-container').attr('id').replace("infowindow-", "")
  $("#game_markers_attributes_#{removedItemId}_latitude").closest(".marker-fields")
    .find(".remove_fields").click()
  delete insertedMarkers[removedItemId]

$(document).on 'click', '.btn-save-marker', ()->
  infowindowContentContainer = $(this).closest('.infowindow-content-container')
  affectedMarkerFieldId = infowindowContentContainer.attr('id').replace("infowindow-", "")
  newMarkerType = infowindowContentContainer.find(".existing-marker-marker-type").val()
  $("#game_markers_attributes_#{affectedMarkerFieldId}_marker_type").val newMarkerType
  marker_icon = [newMarkerType, "marker"].join("_")
  insertedMarkers[affectedMarkerFieldId].setIcon(icons[marker_icon][0])
  infowindow.close()

$(document).ready ->
  controller = $("body").data("controller")
  action = $("body").data("action")

  if controller is "games"
    if action is "new" || action is "edit" || action is "create"
      resetForm()
      initializeFormPage()

    else if action is "index" || action is "show"
      initializeIndexPage()
      