collection @users, root: :users, object_root: false

attributes :id, :email, :role, :sign_in_count, :current_sign_in_at, :last_sign_in_at, 
           :current_sign_in_ip, :last_sign_in_ip, :created_at, :updated_at