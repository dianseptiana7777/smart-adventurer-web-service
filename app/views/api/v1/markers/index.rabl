collection @markers, root: :markers, object_root: false

attributes :id, :latitude, :longitude, :marker_type

child(:game) { attributes :id, :name }
child(:subject) { attributes :id, :name }