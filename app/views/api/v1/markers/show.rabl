object @marker
attributes :id, :latitude, :longitude, :marker_type

child(:game){ attributes :id, :name }
child(:subject){ attributes :id, :name }