collection @subjects

attributes :id, :name, :description

child :school_grade do 
  attributes :id, :grade, :name
  child :school_type do
    attributes :id, :name
  end
end