collection @school_grades

attributes :id, :grade, :name
child :school_type do
  attributes :id, :name
end