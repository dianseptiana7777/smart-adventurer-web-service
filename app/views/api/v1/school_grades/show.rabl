object @school_grade

attributes :id, :grade, :name
child :school_type do
  attributes :id, :name
end