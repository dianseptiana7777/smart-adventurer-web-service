object @question
attributes :id, :question_text, :choice_a, :choice_b, :choice_c, :choice_d, :choice_e,
           :correct_answer, :difficulty_level, :is_public

child(:user) { attribute :id, :email }
child(:school_grade) { attribute :id, :name }
child(:subject) { attribute :id, :name }
node(:duration){ |question| question.duration.strftime("%H:%M:%S") }