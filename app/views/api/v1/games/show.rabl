object @game
attributes :id, :name, :pin, :address, :game_type, :use_password, :password 

child(:user, object_root: false) { attribute :id, :email }
child :markers, object_root: false do 
  attributes :id, :latitude, :longitude, :marker_type
  child(:subject){ attributes :id, :name }
end