class Game < ActiveRecord::Base
  belongs_to :school
  belongs_to :user
  has_many :markers, dependent: :destroy

  accepts_nested_attributes_for :markers, :reject_if => :all_blank, :allow_destroy => true

  before_create :generate_pin
  before_save :encrypt_password

  validates :name, :address, presence: true
  validates :game_type, presence: { message: 'please select game type' }
  validates :password, presence: true, if: Proc.new { |game| game.use_password && game.new_record? }
  validates_confirmation_of :password, if: Proc.new { |game| game.use_password && game.changed.include?("password") }
  validate :validate_markers

  def generate_pin
    self.pin = Time.now.strftime("%d%m%Y%H%M%S")
  end

  def validate_markers
    if markers.empty?
      errors.add(:markers, "You should put one start and finish marker, and at least two regular markers")
    else
      start_finish_markers = markers.group_by { |marker| ["start", "finish"].include? marker.marker_type }.values
      regular_markers = markers.group_by { |marker| !["start", "finish"].include?(marker.marker_type) }.values
      if !start_finish_markers.length.eql?(2) && !(regular_markers.length > 1)
        errors.add(:markers, "You should add one start and finish marker, and at least two regular markers")
      end
    end
  end

  private
  def encrypt_password
    self.password = if use_password
                      if !self.new_record? && self.changed.include?('password') && self.password.blank?
                        self.changes[:password].first
                      else
                        BCrypt::Password.create(password)
                      end
                    end
  end
end
