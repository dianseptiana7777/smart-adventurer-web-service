class Question < ActiveRecord::Base
  belongs_to :user
  belongs_to :school_grade
  belongs_to :subject

  validates :question_text, :choice_a, :choice_b, :choice_c, :choice_d, :correct_answer, presence: true
  validates :difficulty_level, :school_grade_id, presence: { message: 'Please select one' }
  validate :duration_check

  def correct_answer_text
    [correct_answer.upcase, eval("choice_#{correct_answer.downcase}")].join('. ')
  end

  def formatted_duration
    duration.strftime("%H:%M:%S")
  end

  def duration_in_seconds
    hours, minutes, seconds = formatted_duration.split(":").map(&:to_i)
    (hours * 3600) + (minutes * 60) + seconds
  end

  private
  def duration_check
    errors.add(:duration, "The duration shouldn't be all zero") if formatted_duration.eql?("00:00:00")
  end
end