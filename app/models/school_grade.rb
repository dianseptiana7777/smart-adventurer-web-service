class SchoolGrade < ActiveRecord::Base
  belongs_to :school_type

  def self.get_collection
    self.all.map { |school_grade| [school_grade.name, school_grade.id] }
  end

  def name
    "#{grade.ordinalize} grade of #{school_type.name}"
  end
end
