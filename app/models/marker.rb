class Marker < ActiveRecord::Base
  belongs_to :game
  belongs_to :subject
end
