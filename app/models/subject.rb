class Subject < ActiveRecord::Base
  belongs_to :school_grade

  def self.all_by_grade_id(grade_id)
    self.where(school_grade_id: grade_id)
  end

  def self.collection_by_grade_id(grade_id)
    self.all_by_grade_id(grade_id).map { |subject| [subject.name, subject.id] }
  end
end
