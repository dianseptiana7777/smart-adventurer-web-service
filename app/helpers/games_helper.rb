module GamesHelper
  def marker_type_collection(game)
    marker_types = %w(gold question start finish)
    if game
      marker_types - game.markers.map(&:marker_type).select { |marker_type| ["start", "finish"].include? marker_type }
    else
      marker_types
    end
  end
end
