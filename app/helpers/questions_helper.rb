module QuestionsHelper
  def set_choice(correct_answer, current_answer)
    if correct_answer && correct_answer.downcase.eql?(current_answer)
      'has-success'
    end
  end

  def set_difficulty_label(difficulty)
    label = case difficulty
              when 'Easy'
                "<h3 style=\"margin-top: 5px;\"><div class=\"label label-success\">#{difficulty}</div><h3>"
              when 'Medium'
                "<h3 style=\"margin-top: 5px;\"><div class=\"label label-info\">#{difficulty}</div><h3>"
              when 'Hard'
                "<h3 style=\"margin-top: 5px;\"><div class=\"label label-danger\">#{difficulty}</div><h3>"
            end

    label.html_safe
  end
end
