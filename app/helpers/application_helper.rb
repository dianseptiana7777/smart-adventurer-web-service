module ApplicationHelper
  def is_active?(current_controller_name)
    'active' if controller_name.eql?(current_controller_name)
  end
end
