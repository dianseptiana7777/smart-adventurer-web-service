class Api::V1::SchoolTypesController < ApplicationController
  def index
    @school_types = SchoolType.all
  end

  def show
    @school_type = SchoolType.find(params[:id])
  end
end