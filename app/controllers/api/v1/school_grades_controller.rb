class Api::V1::SchoolGradesController < ApplicationController
  def index
    @school_grades = SchoolGrade.all
  end

  def show
    @school_grade = SchoolGrade.find(params[:id])
  end
end