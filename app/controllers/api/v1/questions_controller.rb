class Api::V1::QuestionsController < ApplicationController
  def index
    @questions = Question.all
  end

  def show
    @question = Question.find(params[:id])
  end

  def get_related
    @questions = Question.where('subject_id = ? AND school_grade_id = ? AND difficulty_level = ? 
      AND (user_id = ? OR is_public IS TRUE)', params[:subject_id], params[:school_grade_id],
      params[:difficulty_level], params[:user_id])

    if @questions.empty?
      @questions = Question.where('subject_id = ? AND school_grade_id = ? 
        AND (user_id = ? OR is_public IS TRUE)', params[:subject_id], params[:school_grade_id],
        params[:user_id])
    end

    if @questions.empty?
      @questions = Question.where('user_id = ? OR is_public IS TRUE', params[:user_id])
    end

    render 'index'
  end

  private
  # Never trust parameters from the scary internet, only allow the white list through.
  def question_params
    params.require(:question).permit(:question_text, :choice_a, :choice_b, :choice_c, :choice_d,
      :choice_e, :correct_answer, :school_grade_id, :subject_id, :difficulty_level, :duration, :is_public)
  end
end