class Api::V1::GamesController < Api::V1::ApplicationController
  before_action :set_game, only: [:show, :update, :destroy, :check_password]

  def index
    @games = Game.all
  end

  def create
    @game = Game.new(game_params)

    if @game.save
      render 'show'
    else
      render json: @game.errors, status: :unprocessable_entity
    end
  end

  def update
    if @game.update(game_params)
      render 'show'
    else
      render json: @game.errors, status: :unprocessable_entity
    end
  end

  def show; end

  def destroy
    @game.destroy
    render json: { info: 'Game successfully deleted' }, status: :ok
  end

  def by_pin
    @game = Game.where(pin: params[:pin]).first
    render 'api/v1/games/show'
  end

  def check_password
    if @game && @game.use_password
      decrypted_password = BCrypt::Password.new(@game.password)

      if decrypted_password == params[:password]
        render 'api/v1/games/show'
      else
        render json: { error: 'Wrong password' }, status: :ok
      end
    else
      render 'api/v1/games/show'
    end
  end 

  def list_names
    @games = Game.select(:id, :name, :address, :pin)
  end

  private
  def set_game
    @game = Game.find(params[:id]) rescue nil
  end
  # Never trust parameters from the scary internet, only allow the white list through.
  def game_params
    params.require(:game).permit(:name, :pin, :address, :password, :password_confirmation, 
      :use_password, :game_type, markers_attributes: [:id, :marker_type, :latitude, :longitude, :_destroy])
  end
end