class Api::V1::MarkersController < ApplicationController
  def index
    @markers = Marker.all
  end

  def show
    @marker = Marker.find(params[:id])
  end
end