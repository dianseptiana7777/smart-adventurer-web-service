class HomeController < ApplicationController
  def index
    user_agent = request.env['HTTP_USER_AGENT']
    @transport = (user_agent.nil? || user_agent.match(/MSIE/)) ? 'iframe' : 'xhr'
  end
end
