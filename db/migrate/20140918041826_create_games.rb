class CreateGames < ActiveRecord::Migration
  def change
    create_table :games do |t|
      t.string :name
      t.string :address
      t.string :pin
      t.string :password
      t.string :game_type
      t.boolean :use_password
      t.references :user, index: true

      t.timestamps
    end
  end
end
