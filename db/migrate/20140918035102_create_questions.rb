class CreateQuestions < ActiveRecord::Migration
  def change
    create_table :questions do |t|
      t.text :question_text
      t.string :choice_a
      t.string :choice_b
      t.string :choice_c
      t.string :choice_d
      t.string :choice_e
      t.string :correct_answer, limit: 1
      t.string :difficulty_level
      t.time   :duration
      t.boolean :is_public, default: true
      t.references :user, index: true
      t.references :school_grade, index: true
      t.references :subject, index: true

      t.timestamps
    end
  end
end
