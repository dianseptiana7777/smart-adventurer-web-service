class CreateSchoolGrades < ActiveRecord::Migration
  def change
    create_table :school_grades do |t|
      t.integer :grade
      t.references :school_type, index: true

      t.timestamps
    end
  end
end
