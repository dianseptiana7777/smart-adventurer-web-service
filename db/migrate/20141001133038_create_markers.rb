class CreateMarkers < ActiveRecord::Migration
  def change
    create_table :markers do |t|
      t.float :latitude
      t.float :longitude
      t.string :marker_type
      t.references :game, index: true
      t.references :subject, index: true

      t.timestamps
    end
  end
end