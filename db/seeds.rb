# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)

puts 'Recreate school types'
SchoolType.destroy_all
ActiveRecord::Base.connection.reset_pk_sequence!('school_types')

SchoolType.create([
  { name: 'Sekolah Menengah Atas' },
  { name: 'Sekolah Menengah Pertama' }
])


puts 'Recreate school grades'
SchoolGrade.destroy_all
ActiveRecord::Base.connection.reset_pk_sequence!('school_grades')

SchoolGrade.create([
  { grade: 3, school_type_id: 1 },
  { grade: 1, school_type_id: 1 },
  { grade: 3, school_type_id: 2 },
  { grade: 1, school_type_id: 2 }
])


puts 'Recreate subjects'
Subject.destroy_all
ActiveRecord::Base.connection.reset_pk_sequence!('subjects')

Subject.create([
  { name: 'Kimia', description: 'Ilmu tentang senyawa, materi, dan komposisinya', school_grade_id: 1 },
  { name: 'PKN', description: 'Ilmu tentang kewarganegaraan dan nasionalisme', school_grade_id: 2 },
  { name: 'Fisika', description: 'Ilmu tentang perubahan wujud dzat', school_grade_id: 3 },
  { name: 'Ilmu Pengetahuan Sosial', description: 'Ilmu pengetahuan tentang geografis, kebudayaan, dan sejarah indonesia', school_grade_id: 4 },
  { name: 'Biologi', description: 'Ilmu tentang mahluk hidup', school_grade_id: 4 }
])


puts 'Recreate users'
User.destroy_all
ActiveRecord::Base.connection.reset_pk_sequence!('users')

User.create({ 
  email: "dian@41studio.com", password: "salamnunggal90", password_confirmation: "salamnunggal90",
  reset_password_token: nil, reset_password_sent_at: nil, remember_created_at: nil, sign_in_count: 19,
  current_sign_in_at: "Mon, 27 Oct 2014 03:21:16 UTC +00:00", last_sign_in_at: "Mon, 27 Oct 2014 03:12:54 UTC +00:00", 
  current_sign_in_ip: "127.0.0.1", last_sign_in_ip: "127.0.0.1", confirmation_token: nil, 
  confirmed_at: "Fri, 03 Oct 2014 07:35:20 UTC +00:00", confirmation_sent_at: "Fri, 03 Oct 2014 07:35:01 UTC +00:00",
  unconfirmed_email: nil, role: "contributor" 
})


puts 'Recreate questions'
Question.destroy_all
ActiveRecord::Base.connection.reset_pk_sequence!('questions')

Question.create({ 
  question_text: "The process that used by plant to produce their food is called?", 
  choice_a: "chlorophyll", choice_b: "photosynthesis", choice_c: "osmosis", choice_d: "chemotaxis", choice_e: nil,
  correct_answer: "b", difficulty_level: "Medium", duration: "00:00:01", is_public: true, user_id: 1,
  school_grade_id: 3, subject_id: 3
})
